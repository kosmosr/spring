package top.mollysu.sell.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import top.mollysu.sell.enums.ProductStatusEnum;
import top.mollysu.sell.pojo.ProductInfo;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void fineOne() {
        ProductInfo result = productService.findOne("1");
        Assert.assertTrue(result != null);
    }

    @Test
    public void findUpAll() {
        List<ProductInfo> productInfoList = productService.findUpAll();
        Assert.assertTrue(productInfoList.size() > 0);
    }

    @Test
    public void findAll() {
        Pageable pageable = new PageRequest(0,10);
        Page<ProductInfo> productInfoPage = productService.findAll(pageable);
        Assert.assertTrue(productInfoPage.getTotalElements() > 0);
//        System.out.println(productInfoPage.getTotalElements());
    }

    @Test
    public void save() {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId("2");
        productInfo.setProductName("皮皮虾");
        productInfo.setProductPrice(new BigDecimal(40.0));
        productInfo.setProductStock(233);
        productInfo.setProductDescription("海运大虾");
        productInfo.setProductIcon("http://xxxx.jpg");
        productInfo.setCategoryType(1);

        ProductInfo result = productService.save(productInfo);
        Assert.assertTrue(result != null);
    }

    @Test
    public void onSale() {
        String productId = "1";
        ProductInfo productInfo = productService.onSale(productId);
        Assert.assertTrue(productInfo.getProductStatus().equals(ProductStatusEnum.UP.getCode()));
    }
    @Test
    public void offSale() {
        String productId = "1";
        ProductInfo productInfo = productService.offSale(productId);
        Assert.assertTrue(productInfo.getProductStatus().equals(ProductStatusEnum.DOWN.getCode()));
    }
}