package top.mollysu.sell.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.mollysu.sell.pojo.SellerInfo;

import static org.junit.Assert.*;

/**
 * @Author kosmosr
 * @Date 2018/3/16
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SellerServiceTest {

    @Autowired
    private SellerService sellerService;

    @Test
    public void findSellerByOpenid() {
        SellerInfo result = sellerService.findSellerByOpenid("abc");
        Assert.assertNotNull(result);
    }
}