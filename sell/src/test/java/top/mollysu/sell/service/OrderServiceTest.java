package top.mollysu.sell.service;

import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import top.mollysu.sell.dto.OrderDTO;
import top.mollysu.sell.pojo.OrderDetail;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@Log4j2
public class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @Test
    public void create() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName("张三");
        orderDTO.setBuyerPhone("18868822111");
        orderDTO.setBuyerAddress("慕课网总部");
        orderDTO.setBuyerOpenid("ew3euwhd7sjw9diwkq");

        List<OrderDetail> orderDetailList = new ArrayList<>();
        OrderDetail o1 = new OrderDetail();
        OrderDetail o2 = new OrderDetail();
        o1.setProductId("1");
        o1.setProductQuantity(2);
        o2.setProductId("2");
        o2.setProductQuantity(5);

        orderDetailList.add(o1);
        orderDetailList.add(o2);
        orderDTO.setOrderDetailList(orderDetailList);

        OrderDTO result = orderService.create(orderDTO);
        log.info("【创建订单】result={}", result);
    }

    @Test
    public void findOne() {
        OrderDTO result = orderService.findOne("1519828418698");
        log.info("【查找订单】result={}", result);
        Assert.assertNotNull(result);
    }

    @Test
    public void findAll() {
        Pageable pageable = new PageRequest(0,10);
        String buyerOpenid = "ew3euwhd7sjw9diwkq";
        Page<OrderDTO> orderDTOPage = orderService.findAll(buyerOpenid, pageable);
        Assert.assertTrue(orderDTOPage.getTotalElements() > 0);
    }

    @Test
    public void cancel() {
    }

    @Test
    public void finish() {
    }

    @Test
    public void paid() {
    }
}