package top.mollysu.sell.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.mollysu.sell.pojo.ProductCategory;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceTest {

    @Autowired
    private CategoryService categoryService;

    @Test
    public void findOne() {
        ProductCategory result = categoryService.findOne(1);
        Assert.assertTrue(result != null);
    }

    @Test
    public void findAll() {
        List<ProductCategory> categoryList = categoryService.findAll();
        Assert.assertTrue(categoryList.size() > 0);
    }

    @Test
    public void findByCategoryTypeIn() {
        List<ProductCategory> categoryList = categoryService.findByCategoryTypeIn(Arrays.asList(1, 2, 3, 4));
        Assert.assertTrue(categoryList.size() > 0);
    }

    @Test
    public void update() {
        ProductCategory category = categoryService.findOne(2);
        category.setCategoryName("男生最爱");
        ProductCategory result = categoryService.save(category);
        Assert.assertTrue(result != null);
    }

    @Test
    public void save() {
        ProductCategory category = new ProductCategory();
        category.setCategoryName("女生最爱");
        category.setCategoryType(3);

        ProductCategory result = categoryService.save(category);
        Assert.assertTrue(result != null);
    }
}