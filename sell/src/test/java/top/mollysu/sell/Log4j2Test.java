package top.mollysu.sell;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class Log4j2Test {

    private static String one = "One";
    private static String two = "Two";

    @Test
    public void test() {
        log.info("info test...");
        log.debug("debug test...");
        log.error("error test...");
        log.info("one = {}, two = {}", one, two);
    }
}
