package top.mollysu.sell.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.mollysu.sell.pojo.SellerInfo;
import top.mollysu.sell.util.KeyUtils;
import top.mollysu.sell.util.PasswordUtils;

import static org.junit.Assert.*;

/**
 * @Author kosmosr
 * @Date 2018/3/16
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SellerInfoRepositoryTest {

    @Autowired
    private SellerInfoRepository repository;

    @Test
    public void save() {
        SellerInfo sellerInfo = new SellerInfo();
        sellerInfo.setSellerId(KeyUtils.genUniqueKey());
        sellerInfo.setUsername("admin");
        sellerInfo.setPassword(PasswordUtils.encode("admin"));
        sellerInfo.setOpenid("abc");
        SellerInfo result = repository.save(sellerInfo);
        Assert.assertNotNull(result);
    }

    @Test
    public void update() {
        SellerInfo admin = repository.findByUsername("admin");
        admin.setPassword(PasswordUtils.encode("admin"));
        SellerInfo result = repository.save(admin);
        Assert.assertNotNull(result);
    }

    @Test
    public void findByOpenid() {
        SellerInfo result = repository.findByOpenid("abc");
        Assert.assertTrue(result != null);
    }


}