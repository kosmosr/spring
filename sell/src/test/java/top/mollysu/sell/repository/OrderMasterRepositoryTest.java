package top.mollysu.sell.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import top.mollysu.sell.pojo.OrderMaster;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderMasterRepositoryTest {

    private final String OPENID = "110110";

    @Autowired
    private OrderMasterRepository repository;

    @Test
    public void saveTest() {
        OrderMaster orderMaster = new OrderMaster();
        orderMaster.setOrderId("123456");
        orderMaster.setBuyerName("test名字");
        orderMaster.setBuyerAddress("test地址");
        orderMaster.setBuyerPhone("124567890123");
        orderMaster.setBuyerOpenid(OPENID);
        orderMaster.setOrderAmount(new BigDecimal(50.5));

        OrderMaster result = repository.save(orderMaster);
        Assert.assertTrue(result != null);
    }

    @Test
    public void findByBuyerOpenId() {
        Pageable pageable = new PageRequest(0, 10);
        Page<OrderMaster> orderMasterPage = repository.findByBuyerOpenid(OPENID, pageable);

        Assert.assertTrue(orderMasterPage.getTotalElements() != 0);
    }
}