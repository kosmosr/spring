var websocket = null;
if ('WebSocket' in window) {
    websocket = new WebSocket('ws://localhost:8080/webSocket');
} else {
    alert("该浏览器不支持WebSocket");
}

websocket.onopen = function () {
    console.log("建立连接");
};

websocket.onclose = function () {
    console.log("连接关闭");
};

websocket.onmessage = function (event) {
    console.log("收到消息: " + event.data);
    if (event.data.code == 0) {
        alert("当前未登录");
    }
};

websocket.onerror = function () {
    alert("通信发送错误!");
};

