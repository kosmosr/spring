package top.mollysu.sell.service;

import top.mollysu.sell.dto.OrderDTO;

public interface BuyerService {
    OrderDTO findOrderOne(String openid, String orderId);

    OrderDTO cancel(String openid, String orderId);
}
