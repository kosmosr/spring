package top.mollysu.sell.service;

import com.lly835.bestpay.model.PayResponse;
import top.mollysu.sell.dto.OrderDTO;

public interface PayService {
    PayResponse create(OrderDTO orderDTO);
}
