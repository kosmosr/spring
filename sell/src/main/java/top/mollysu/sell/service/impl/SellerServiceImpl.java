package top.mollysu.sell.service.impl;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.mollysu.sell.enums.ResultEnum;
import top.mollysu.sell.exception.SellException;
import top.mollysu.sell.pojo.SellerInfo;
import top.mollysu.sell.repository.SellerInfoRepository;
import top.mollysu.sell.service.SellerService;

/**
 * @Author kosmosr
 * @Date 2018/3/16
 */
@Service
@Log4j2
public class SellerServiceImpl implements SellerService {

    @Autowired
    private SellerInfoRepository sellerInfoRepository;


    @Override
    public SellerInfo findSellerByOpenid(String openid) {
        return sellerInfoRepository.findByOpenid(openid);
    }

    @Override
    public SellerInfo findSellerByUsername(String username) {
        SellerInfo sellerInfo = sellerInfoRepository.findByUsername(username);
        return sellerInfo;
    }

    @Override
    public SellerInfo findSellerBySellerId(String sellerId) {
        return sellerInfoRepository.findOne(sellerId);
    }
}
