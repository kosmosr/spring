package top.mollysu.sell.service;

import top.mollysu.sell.pojo.SellerInfo;

/**
 * @Author kosmosr
 * @Date 2018/3/16
 */
public interface SellerService {

    /**
     * 根据openid查找卖家
     * @param openid 微信用户id
     * @return 卖家端信息
     */
    SellerInfo findSellerByOpenid(String openid);

    /**
     * 根据用户名查询卖家
     * @param username
     * @return
     */
    SellerInfo findSellerByUsername(String username);

    SellerInfo findSellerBySellerId(String sellerId);
}
