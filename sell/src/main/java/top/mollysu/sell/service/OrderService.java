package top.mollysu.sell.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.mollysu.sell.dto.OrderDTO;

public interface OrderService {

    /**
     * 创建订单
     *
     * @param orderDTO
     * @return
     */
    OrderDTO create(OrderDTO orderDTO);

    /**
     * 查询单个订单
     *
     * @param orderId
     * @return
     */
    OrderDTO findOne(String orderId);


    /**
     * 查询订单列表
     *
     * @param buyerOpenid 微信id
     * @param pageable    分页参数
     * @return
     */
    Page<OrderDTO> findAll(String buyerOpenid, Pageable pageable);

    /**
     * 查询所有订单列表 卖家端使用
     * @param pageable
     * @return
     */
    Page<OrderDTO> findAll(Pageable pageable);

    /**
     * 取消订单
     *
     * @param orderDTO
     * @return
     */
    OrderDTO cancel(OrderDTO orderDTO);

    /**
     * 完结订单
     *
     * @param orderDTO
     * @return
     */
    OrderDTO finish(OrderDTO orderDTO);

    /**
     * 支付订单
     *
     * @param orderDTO
     * @return
     */
    OrderDTO paid(OrderDTO orderDTO);

}
