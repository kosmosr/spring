package top.mollysu.sell.service;

import top.mollysu.sell.pojo.ProductCategory;

import java.util.List;

public interface CategoryService {

    /**
     * 根据类目id查询
     * @param categoryId 类目id
     * @return
     */
    ProductCategory findOne(Integer categoryId);

    /**
     * 查询所有类目
     * @return
     */
    List<ProductCategory> findAll();

    /**
     * 根据类目编号查询
     * @param categoryTypeList 类目编号
     * @return
     */
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);

    /**
     * 新增 OR 更新
     * @param productCategory
     * @return
     */
    ProductCategory save(ProductCategory productCategory);
}
