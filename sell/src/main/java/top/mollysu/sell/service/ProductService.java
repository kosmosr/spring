package top.mollysu.sell.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.mollysu.sell.dto.CartDTO;
import top.mollysu.sell.pojo.ProductInfo;

import java.util.List;

public interface ProductService {

    ProductInfo findOne(String productId);

    /**
     * 查询所有在架商品列表
     *
     * @return
     */
    List<ProductInfo> findUpAll();


    Page<ProductInfo> findAll(Pageable pageable);

    ProductInfo save(ProductInfo productInfo);

    /**
     * 加库存
     *
     * @param cartDTOList 购物车
     */
    void increaseStock(List<CartDTO> cartDTOList);

    /**
     * 减库存
     *
     * @param cartDTOList 购物车
     */
    void decreaseStock(List<CartDTO> cartDTOList);

    /**
     * 下架商品
     * @param productId
     * @return
     */
    ProductInfo offSale(String productId);

    /**
     * 上架商品
     * @param productId
     * @return
     */
    ProductInfo onSale(String productId);
}
