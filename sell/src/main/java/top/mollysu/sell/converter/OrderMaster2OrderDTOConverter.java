package top.mollysu.sell.converter;

import org.springframework.beans.BeanUtils;
import top.mollysu.sell.dto.OrderDTO;
import top.mollysu.sell.pojo.OrderMaster;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderMaster2OrderDTOConverter {

    public static OrderDTO convert(OrderMaster orderMaster) {
        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(orderMaster, orderDTO);
        return orderDTO;
    }

    public static List<OrderDTO> convert(List<OrderMaster> orderMasterList) {
        /*
        传统实现
        List<OrderDTO> orderDTOList = new ArrayList<>();
        for (OrderMaster orderMaster : orderMasterList) {
            OrderDTO orderDTO = new OrderDTO();
            BeanUtils.copyProperties(orderMaster, orderDTO);
            orderDTOList.add(orderDTO);
        }

        return orderDTOList;
        */

        return orderMasterList.stream().map(e -> convert(e)).collect(Collectors.toList());
    }
}
