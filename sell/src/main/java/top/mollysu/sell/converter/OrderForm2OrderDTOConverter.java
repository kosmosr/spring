package top.mollysu.sell.converter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import top.mollysu.sell.dto.OrderDTO;
import top.mollysu.sell.form.OrderForm;
import top.mollysu.sell.pojo.OrderDetail;

import java.util.List;

public class OrderForm2OrderDTOConverter {

    public static OrderDTO convert(OrderForm orderForm) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName(orderForm.getName());
        orderDTO.setBuyerPhone(orderForm.getPhone());
        orderDTO.setBuyerOpenid(orderForm.getOpenid());
        orderDTO.setBuyerAddress(orderForm.getAddress());
        orderDTO.setOrderDetailList(JSON.parseObject(orderForm.getItems(), new TypeReference<List<OrderDetail>>(){}));

        return orderDTO;
    }
}
