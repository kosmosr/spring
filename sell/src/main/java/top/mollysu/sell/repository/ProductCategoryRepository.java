package top.mollysu.sell.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.mollysu.sell.pojo.ProductCategory;

import java.util.List;

/**
 * 类目dao
 */
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);
}
