package top.mollysu.sell.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.mollysu.sell.pojo.OrderDetail;

import java.util.List;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, String> {

    List<OrderDetail> findByOrderId(String orderId);
}
