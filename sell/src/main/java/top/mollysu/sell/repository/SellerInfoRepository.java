package top.mollysu.sell.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.mollysu.sell.pojo.SellerInfo;

/**
 * @Author kosmosr
 * @Date 2018/3/16
 */
public interface SellerInfoRepository extends JpaRepository<SellerInfo, String> {

    /**
     * 根据用户微信id查询
     * @param openid 微信id
     * @return
     */
    SellerInfo findByOpenid(String openid);

    /**
     * 根据用户名查询
     * @param username 用户名
     * @return 卖家
     */
    SellerInfo findByUsername(String username);
}
