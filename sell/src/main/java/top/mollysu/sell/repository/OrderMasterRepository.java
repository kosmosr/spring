package top.mollysu.sell.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import top.mollysu.sell.pojo.OrderMaster;

public interface OrderMasterRepository extends JpaRepository<OrderMaster, String> {

    /**
     * 分页查询订单
     * @param buyerOpenid 微信id
     * @param pageable 分页参数
     * @return
     */
    Page<OrderMaster> findByBuyerOpenid(String buyerOpenid, Pageable pageable);
}
