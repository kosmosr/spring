package top.mollysu.sell.util;

import top.mollysu.sell.enums.CodeEnum;
import top.mollysu.sell.enums.OrderStatusEnum;

public class EnumUtil {

    public static <T extends CodeEnum> T getcode(Integer code, Class<T> enumClass) {
        for (T each : enumClass.getEnumConstants()) {
            if (code.equals(each.getCode())) {
                return each;
            }
        }
        return null;
    }
}
