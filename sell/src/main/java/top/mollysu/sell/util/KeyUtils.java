package top.mollysu.sell.util;

import org.apache.commons.lang3.RandomUtils;

import java.util.Random;

public class KeyUtils {

    /**
     * 生成唯一主键
     *
     * @return
     */
    public static synchronized String genUniqueKey() {
        return String.valueOf(System.currentTimeMillis() + RandomUtils.nextInt(100000, 1000000));
    }
}
