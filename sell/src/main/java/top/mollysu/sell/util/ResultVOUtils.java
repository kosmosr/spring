package top.mollysu.sell.util;

import top.mollysu.sell.VO.ResultVO;

public class ResultVOUtils {

    public static ResultVO success(Object o) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(0);
        resultVO.setMsg("成功");
        resultVO.setData(o);
        return resultVO;
    }

    public static ResultVO success() {
        return success(null);
    }

    public static ResultVO error(String msg) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(-1);
        resultVO.setMsg(msg);
        return resultVO;
    }

    public static ResultVO commonError(Object o){
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(-100);
        resultVO.setData(o);
        return resultVO;
    }
}
