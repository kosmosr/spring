package top.mollysu.sell.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import top.mollysu.sell.exception.SellAuthorizeException;
import top.mollysu.sell.service.WebSocket;
import top.mollysu.sell.util.ResultVOUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author kosmosr
 * @Date 2018/3/25
 */
@ControllerAdvice
public class SellExceptionHandler {

    @Autowired
    private WebSocket webSocket;

    @ExceptionHandler(SellAuthorizeException.class)
    public ModelAndView handlerAuthorizeException() {
        webSocket.sendMessage(ResultVOUtils.success());
        return new ModelAndView("user/login.btl");
    }
}
