package top.mollysu.sell.VO;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ProductInfoVo implements Serializable {

    private static final long serialVersionUID = -2637922864659130799L;

    @JSONField(name = "id")
    private String productId;

    @JSONField(name = "name")
    private String productName;

    @JSONField(name = "price")
    private BigDecimal productPrice;

    @JSONField(name = "description")
    private String productDescription;

    @JSONField(name = "icon")
    private String productIcon;


}
