package top.mollysu.sell.VO;

import lombok.Data;

import java.io.Serializable;

/**
 * 结果视图
 */
@Data
public class ResultVO<T> implements Serializable {

    private static final long serialVersionUID = 837343822827597330L;

    /** 状态码 */
    private Integer code;

    /** 状态信息 */
    private String msg;

    /**  内容 */
    private T data;
}
