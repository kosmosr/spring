package top.mollysu.sell.enums;

public interface CodeEnum {
    Integer getCode();
}
