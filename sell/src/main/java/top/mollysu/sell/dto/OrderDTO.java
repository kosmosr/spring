package top.mollysu.sell.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import top.mollysu.sell.enums.OrderStatusEnum;
import top.mollysu.sell.enums.PayStatusEnum;
import top.mollysu.sell.pojo.OrderDetail;
import top.mollysu.sell.util.EnumUtil;
import top.mollysu.sell.util.serializer.Date2LongSerialize;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrderDTO {

    /**
     * 订单id
     */
    @Id
    private String orderId;

    /**
     * 买家姓名
     */
    private String buyerName;

    /**
     * 买家电话
     */
    private String buyerPhone;

    /**
     * 买家地址
     */
    private String buyerAddress;

    /**
     * 买家微信id
     */
    private String buyerOpenid;

    /**
     * 订单总额
     */
    private BigDecimal orderAmount;

    /**
     * 订单状态 默认0 新下单
     */
    private Integer orderStatus;

    /**
     * 支付状态 默认0 未支付
     */
    private Integer payStatus;

    @JSONField(serializeUsing = Date2LongSerialize.class)
    private Date createTime;

    @JSONField(serializeUsing = Date2LongSerialize.class)
    private Date updateTime;

    private List<OrderDetail> orderDetailList;

    @JSONField(serialize = false)
    public PayStatusEnum getPayStatusEnum() {
        return EnumUtil.getcode(payStatus, PayStatusEnum.class);
    }

    @JSONField(serialize = false)
    public OrderStatusEnum getOrderStatusEnum() {
        return EnumUtil.getcode(orderStatus, OrderStatusEnum.class);
    }
}
