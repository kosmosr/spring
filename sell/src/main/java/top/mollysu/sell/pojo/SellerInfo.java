package top.mollysu.sell.pojo;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * @Author kosmosr
 * @Date 2018/3/16
 * 卖家信息 实体类
 */
@Entity
@Data
@DynamicUpdate
@DynamicInsert
public class SellerInfo {

    @Id
    private String sellerId;

    private String username;

    private String password;

    private String openid;

    private Date createTime;

    private Date updateTime;
}
