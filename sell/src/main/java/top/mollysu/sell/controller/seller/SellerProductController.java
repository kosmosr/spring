package top.mollysu.sell.controller.seller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import top.mollysu.sell.enums.ResultEnum;
import top.mollysu.sell.exception.SellException;
import top.mollysu.sell.form.ProductForm;
import top.mollysu.sell.pojo.ProductCategory;
import top.mollysu.sell.pojo.ProductInfo;
import top.mollysu.sell.service.CategoryService;
import top.mollysu.sell.service.ProductService;
import top.mollysu.sell.util.KeyUtils;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
@Log4j2
@RequestMapping("/seller/product")
public class SellerProductController {

    private final ProductService productService;

    private final CategoryService categoryService;

    @Autowired
    public SellerProductController(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @GetMapping("/list")
    public ModelAndView list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                             @RequestParam(value = "size", defaultValue = "10") Integer size) {
        ModelAndView modelAndView = new ModelAndView("product/list.btl");
        Pageable pageable = new PageRequest(page - 1, size);
        Page<ProductInfo> productInfoPage = productService.findAll(pageable);
        modelAndView.addObject("productList", productInfoPage.getContent());
        modelAndView.addObject("categoryList", categoryService.findAll());
        modelAndView.addObject("count", productInfoPage.getTotalElements());
        modelAndView.addObject("curr", page);
        modelAndView.addObject("size", size);

        return modelAndView;
    }

    @GetMapping("/off_sale")
    public ModelAndView offSale(@RequestParam("orderId") String orderId, Map<String, Object> map) {
        try {
            productService.offSale(orderId);
        } catch (SellException e) {
            map.put("msg", e.getMessage());
            return new ModelAndView("common/error.btl", map);
        }
        map.put("msg", "下架成功");
        return new ModelAndView("common/success.btl", map);
    }

    @GetMapping("/on_sale")
    public ModelAndView onSale(@RequestParam("orderId") String orderId, Map<String, Object> map) {
        try {
            productService.onSale(orderId);
        } catch (SellException e) {
            map.put("msg", e.getMessage());
            return new ModelAndView("common/error.btl", map);
        }
        map.put("msg", "上架成功");
        return new ModelAndView("common/success.btl", map);
    }

    @GetMapping("/edit")
    public ModelAndView edit(@RequestParam(value = "productId", required = false) String productId,
                             Map<String, Object> map) {
        if (!StringUtils.isEmpty(productId)) {
            ProductInfo productInfo = productService.findOne(productId);
            map.put("product", productInfo);

        }
        List<ProductCategory> productCategoryList = categoryService.findAll();
        map.put("categoryList", productCategoryList);
        return new ModelAndView("product/edit.btl", map);
    }

    @PostMapping("/save")
    public ModelAndView save(@Valid ProductForm form, BindingResult bindingResult, Map<String, Object> map) {
        if (bindingResult.hasErrors()) {
            map.put("msg", bindingResult.getFieldError().getDefaultMessage());
            return new ModelAndView("common/error.btl", map);
        }

        ProductInfo productInfo = new ProductInfo();
        if (StringUtils.isEmpty(form.getProductId())) {
            form.setProductId(KeyUtils.genUniqueKey());
        } else {
            productInfo = productService.findOne(form.getProductId());
        }
        BeanUtils.copyProperties(form, productInfo);
        try {
            productService.save(productInfo);
        } catch (SellException e) {
            map.put("msg", e.getMessage());
            return new ModelAndView("common/error.btl", map);
        }

        map.put("msg", ResultEnum.COMMON_SUCCESS.getMessage());
        return new ModelAndView("common/success.btl", map);
    }

}
