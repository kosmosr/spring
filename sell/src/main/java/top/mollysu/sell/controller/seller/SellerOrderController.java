package top.mollysu.sell.controller.seller;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import top.mollysu.sell.dto.OrderDTO;
import top.mollysu.sell.enums.ResultEnum;
import top.mollysu.sell.exception.SellException;
import top.mollysu.sell.service.OrderService;

import java.util.Map;

@Controller
@RequestMapping("/seller/order")
@Log4j2
public class SellerOrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/list")
    public ModelAndView list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                             @RequestParam(value = "size", defaultValue = "10") Integer size) {
        ModelAndView modelAndView = new ModelAndView("order/list.btl");
        Sort sort = new Sort(Sort.Direction.DESC, "createTime");
        Pageable pageable = new PageRequest(page - 1, size, sort);
        Page<OrderDTO> orderDTOPage = orderService.findAll(pageable);
        modelAndView.addObject("orderDTOList", orderDTOPage.getContent());
        modelAndView.addObject("count", orderDTOPage.getTotalElements());
        modelAndView.addObject("curr", page);
        modelAndView.addObject("size", size);
        return modelAndView;
    }

    @GetMapping("/cancel")
    public ModelAndView cancel(@RequestParam("orderId") String orderId, Map<String, Object> map) {
        OrderDTO orderDTO = null;
        try {
            orderDTO = orderService.findOne(orderId);
            orderService.cancel(orderDTO);
        } catch (SellException e) {
            log.error("【卖家端取消订单】查询不到订单");
            map.put("msg", e.getMessage());
            return new ModelAndView("common/error.btl", map);
        }
        map.put("msg", ResultEnum.ORDER_CANCEL_SUCCESS.getMessage());
        return new ModelAndView("common/success.btl", map);
    }

    @GetMapping("/detail")
    public ModelAndView detail(@RequestParam("orderId") String orderId, Map<String, Object> map) {
        OrderDTO orderDTO = null;
        try {
            orderDTO = orderService.findOne(orderId);
        } catch (SellException e) {
            log.error("【卖家端详情订单】查询不到订单");
            map.put("msg", e.getMessage());
            return new ModelAndView("common/error.btl", map);
        }
        map.put("orderDTO", orderDTO);
        return new ModelAndView("order/detail.btl", map);
    }

    @GetMapping("/finish")
    public ModelAndView finish(@RequestParam("orderId") String orderId, Map<String, Object> map) {
        OrderDTO orderDTO = null;
        try {
            orderDTO = orderService.findOne(orderId);
            orderService.finish(orderDTO);
        } catch (SellException e) {
            log.error("【卖家端完结订单】查询不到订单");
            map.put("msg", e.getMessage());
            return new ModelAndView("common/error.btl", map);
        }
        map.put("msg", ResultEnum.ORDER_FINISHED_SUCCESS.getMessage());
        return new ModelAndView("common/success.btl", map);
    }
}
