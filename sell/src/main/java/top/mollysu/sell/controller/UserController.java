package top.mollysu.sell.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import top.mollysu.sell.VO.ResultVO;
import top.mollysu.sell.form.SellerForm;
import top.mollysu.sell.pojo.SellerInfo;
import top.mollysu.sell.service.SellerService;
import top.mollysu.sell.util.KeyUtils;
import top.mollysu.sell.util.ResultVOUtils;

import javax.validation.Valid;

/**
 * @Author kosmosr
 * @Date 2018/3/17
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private SellerService sellerService;

    @GetMapping("/{id}")
    public ResultVO user(@PathVariable("id") String sellerId){
        SellerInfo sellerInfo = sellerService.findSellerBySellerId(sellerId);
        return ResultVOUtils.success(sellerInfo);
    }

    @PostMapping
    public ResultVO create(@Valid SellerForm form, BindingResult result, CommonsMultipartFile multipartFile) {
        if (result.hasErrors()) {
            return ResultVOUtils.commonError(result.getFieldError().getDefaultMessage());
        }

        SellerInfo sellerInfo = new SellerInfo();
        BeanUtils.copyProperties(form,sellerInfo);
        sellerInfo.setSellerId(KeyUtils.genUniqueKey());
        return ResultVOUtils.success();
    }
}
