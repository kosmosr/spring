package top.mollysu.sell.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import top.mollysu.sell.VO.ResultVO;
import top.mollysu.sell.converter.OrderForm2OrderDTOConverter;
import top.mollysu.sell.dto.OrderDTO;
import top.mollysu.sell.enums.ResultEnum;
import top.mollysu.sell.exception.SellException;
import top.mollysu.sell.form.OrderForm;
import top.mollysu.sell.service.BuyerService;
import top.mollysu.sell.service.OrderService;
import top.mollysu.sell.util.ResultVOUtils;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/buyer/order")
@Log4j2
public class BuyerOrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private BuyerService buyerService;

    /**
     * 创建订单
     *
     * @param orderForm
     * @param bindingResult
     * @return
     */
    @PostMapping("/create")
    public ResultVO<Map<String, String>> create(@Valid OrderForm orderForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            log.error("【创建订单】参数不正确，orderForm={}", orderForm);
            throw new SellException(ResultEnum.PARAM_ERROR.getCode(), bindingResult.getFieldError().getDefaultMessage());
        }

        OrderDTO orderDTO = OrderForm2OrderDTOConverter.convert(orderForm);
        if (CollectionUtils.isEmpty(orderDTO.getOrderDetailList())) {
            log.error("【创建订单】购物车不能为空");
            throw new SellException(ResultEnum.CART_EMPTY);
        }

        OrderDTO result = orderService.create(orderDTO);
        Map<String, String> map = new HashMap<>();
        map.put("orderId", result.getOrderId());

        return ResultVOUtils.success(map);
    }

    @GetMapping("/list")
    public ResultVO<List<OrderDTO>> list(@RequestParam("openid") String openid,
                                         @RequestParam(value = "page", defaultValue = "0") Integer page,
                                         @RequestParam(value = "size", defaultValue = "10") Integer size) {

        if (StringUtils.isEmpty(openid)) {
            log.error("【查询订单列表】openid为空");
            throw new SellException(ResultEnum.PARAM_ERROR);
        }
        Pageable pageable = new PageRequest(page, size, Sort.Direction.ASC);
        Page<OrderDTO> orderDTOPage = orderService.findAll(openid, pageable);
        return ResultVOUtils.success(orderDTOPage.getContent());
    }

    @GetMapping("/detail")
    public ResultVO<OrderDTO> detail(@RequestParam(value = "openid", required = false) String openid,
                                     @RequestParam("orderId") String orderId) {
        OrderDTO result = buyerService.findOrderOne(openid, orderId);
        return ResultVOUtils.success(result);

    }

    @PostMapping("/cancel")
    public ResultVO cancel(@RequestParam(value = "openid", required = false) String openid,
                           @RequestParam("orderId") String orderId) {
        buyerService.cancel(openid, orderId);
        return ResultVOUtils.success();

    }

}
