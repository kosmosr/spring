package com.imooc.o2o.dao;

import com.imooc.o2o.BaseTest;
import com.imooc.o2o.pojo.Area;
import com.imooc.o2o.pojo.PersonInfo;
import com.imooc.o2o.pojo.Shop;
import com.imooc.o2o.pojo.ShopCategory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.*;

public class ShopMapperTest extends BaseTest {

    @Autowired
    private ShopMapper shopMapper;

    @Test
    public void insert() {
        Shop shop = new Shop();

        shop.setOwnerId(1);
        shop.setAreaId(2);
        shop.setShopCategoryId(10);
        shop.setShopName("测试2018");

        int result = shopMapper.insert(shop);
        assertTrue(result > 0);
    }


    @Test
    public void updateByPrimaryKeySelective() {
        Shop shop = shopMapper.selectByPrimaryKey(28);
        shop.setCreateTime(new Date());
        shop.setLastEditTime(new Date());
        shop.setShopDesc("更新描述");

        int result = shopMapper.updateByPrimaryKeySelective(shop);
        assertTrue(result > 0);
    }
}