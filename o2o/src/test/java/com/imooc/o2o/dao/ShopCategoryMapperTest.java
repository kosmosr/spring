package com.imooc.o2o.dao;

import com.imooc.o2o.BaseTest;
import com.imooc.o2o.pojo.ShopCategory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

public class ShopCategoryMapperTest extends BaseTest {

    @Autowired
    private ShopCategoryMapper shopCategoryMapper;

    @Test
    public void selectAll() {
     /*   shopCategory.setShopCategoryId(10);
        shopCategory.setParentId(10);*/
        List<ShopCategory> shopCategoryList = shopCategoryMapper.selectAll(new ShopCategory());
        assertTrue(shopCategoryList.size() > 0);
    }
}