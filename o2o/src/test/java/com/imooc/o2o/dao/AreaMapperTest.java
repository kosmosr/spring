package com.imooc.o2o.dao;

import com.imooc.o2o.BaseTest;
import com.imooc.o2o.dao.split.DynamicDataSourceHolder;
import com.imooc.o2o.pojo.Area;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

public class AreaMapperTest extends BaseTest {

    @Autowired
    private AreaMapper areaMapper;

    @Test
    public void selectAll() {
       // DynamicDataSourceHolder.setDbType(DynamicDataSourceHolder.DB_SLAVE);
        List<Area> areaList = areaMapper.selectAll();
        assertTrue(areaList.size() > 0);
        System.out.println(DynamicDataSourceHolder.getDbType());
    }

    @Test
    public void insert() {
        System.out.println(" 数据源为：" + DynamicDataSourceHolder.getDbType());
        Area area = new Area();
        area.setAreaName("test");
        area.setPriority(0);
        int result = areaMapper.insert(area);
        assertTrue(result > 0);
    }
}