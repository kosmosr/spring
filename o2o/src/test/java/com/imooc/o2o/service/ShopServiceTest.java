package com.imooc.o2o.service;

import com.imooc.o2o.BaseTest;
import com.imooc.o2o.dao.ShopMapper;
import com.imooc.o2o.dto.ShopExecution;
import com.imooc.o2o.pojo.Shop;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static org.junit.Assert.*;

public class ShopServiceTest extends BaseTest {

    @Autowired
    private ShopService shopService;

    @Autowired
    private ShopMapper shopMapper;

    @Test
    public void addShop() throws FileNotFoundException {
        Shop shop = new Shop();

        shop.setOwnerId(1);
        shop.setAreaId(2);
        shop.setShopCategoryId(10);
        shop.setShopName("插入服务测试2.9");

        File shopImg = new File("C:\\Users\\kosmosr\\Pictures\\image\\baoman2.jpg");
        InputStream inputStream = new FileInputStream(shopImg);
        ShopExecution se = shopService.addShop(shop, inputStream, shopImg.getName());
        assertTrue(se.getState() == 0);

    }
}