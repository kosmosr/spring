package com.imooc.o2o.util;

public class PathUtil {

    private static final String SEPARATOR = System.getProperty("file.separator");

    /**
     * 返回根路径
     *
     * @return
     */
    public static String getImgBasePath() {
        String os = System.getProperty("os.name");
        String basePath = null;
        if (os.startsWith("Windows")) {
            basePath = "E:\\o2o\\image\\";
        } else {
            basePath = "/home/o2o/image/";
        }
        return basePath;
    }

    /**
     * 获取商品相对路径
     * @param shopId
     * @return
     */
    public static String getShopImagePath(Integer shopId) {
        String imagePath = "/upload/item/shop/" + shopId + "/";
        return imagePath.replace("/", SEPARATOR);
    }

    public static void main(String[] args) {
        System.out.println(getImgBasePath());
    }
}
