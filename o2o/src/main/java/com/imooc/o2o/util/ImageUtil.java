package com.imooc.o2o.util;

import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Slf4j
public class ImageUtil {

    private static String BASEPATH = Thread.currentThread().getContextClassLoader().getResource("").getPath();
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    public static String generateThumbnail(InputStream multipartFile, String targetAddr, String fileName) {
        String realFileName = getRandomFileName();
        String extension = getFileExtension(fileName);
        mkdirFilePath(targetAddr);
        //相对地址
        String relativeAddr = targetAddr + realFileName + extension;
        log.debug("相对路径：" + relativeAddr);
        //绝对地址
        File dest = new File(PathUtil.getImgBasePath() + relativeAddr);
        log.debug("完整路径:" + dest.getPath());
        try {
            Thumbnails.of(multipartFile)
                    .size(200, 200)
                    .watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File(BASEPATH + "/watermark.jpg")), 0.25f)
                    .outputQuality(0.8f)
                    .toFile(dest);
        } catch (IOException e) {
            log.error(e.toString());
            e.printStackTrace();
        }

        return relativeAddr;
    }

    public static File transforCommonsMultipartFileToFile(CommonsMultipartFile multipartFile) {
        File file = new File(multipartFile.getOriginalFilename());
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            log.error(e.toString());
        }
        return file;
    }

    private static void mkdirFilePath(String targetAddr) {
        String realFileParentPath = PathUtil.getImgBasePath() + targetAddr;
        File dirPath = new File(realFileParentPath);
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }
    }

    private static String getFileExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."));
    }

    private static String getRandomFileName() {
        Random random = new Random();
        int number = random.nextInt(89999) + 10000;
        String nowTimeStr = simpleDateFormat.format(new Date());
        return nowTimeStr + number;
    }

    public static void deleteFileOrPath(String path) {
        File file = new File(PathUtil.getImgBasePath() + path);
        if (file.exists()) {
            if (file.isDirectory()) {
                for (File f : file.listFiles()) {
                    f.delete();
                }
            } else {
                file.delete();
            }

        }
    }

    public static void main(String[] args) throws IOException {
        Thumbnails
                .of(new File("C:\\Users\\kosmosr\\Pictures\\image\\xiaohuangren.jpg"))
                .size(200, 200)
                .watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File(BASEPATH + "/watermark.jpg")), 0.25f)
                .outputQuality(0.8f)
                .toFile(new File("C:\\Users\\kosmosr\\Pictures\\image\\xiaohuangrennew.jpg"));
    }
}
