package com.imooc.o2o.util;

import lombok.Data;

import java.util.Map;

@Data
public class ResultMapUtil<T> {
    private boolean success;

    private String message;

    private T data;

    private Map<String, Object> modelMap;

    private ResultMapUtil(boolean success) {
        this.success = success;
    }

    private ResultMapUtil(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    private ResultMapUtil(boolean success, String message, T data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public static ResultMapUtil Code(boolean success) {
        return new ResultMapUtil(success);
    }

    public static ResultMapUtil CodeAndMsg(boolean success, String message) {
        return new ResultMapUtil(success, message);
    }

    public static <T> ResultMapUtil CodeAndMsgData(boolean success, String message, T data) {
        return new ResultMapUtil(success, message, data);
    }



}
