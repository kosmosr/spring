package com.imooc.o2o.util;

import com.google.code.kaptcha.Constants;

import javax.servlet.http.HttpServletRequest;

public class CodeUtil {

    public static boolean checkVerifyCode(HttpServletRequest request) {
        String capText = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        String verifyCode = request.getParameter("verifyCode");
        if (!capText.equalsIgnoreCase(verifyCode)) {
            return false;
        }

        return true;
    }
}
