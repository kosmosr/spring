package com.imooc.o2o.controller.shopadmin;

import com.imooc.o2o.pojo.Area;
import com.imooc.o2o.pojo.ShopCategory;
import com.imooc.o2o.service.AreaService;
import com.imooc.o2o.service.ShopCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/shopadmin")
public class ShopAdminController {

    @Autowired
    private AreaService areaService;

    @Autowired
    private ShopCategoryService shopCategoryService;

    @RequestMapping("/shopadd")
    public ModelAndView shopAdd() {
        ModelAndView modelAndView = new ModelAndView("shop/addshop");
        modelAndView.addObject("areaList", areaService.getAreaList());
        modelAndView.addObject("shopCategoryList", shopCategoryService.getShopCategoryList(new ShopCategory()));

        return modelAndView;
    }
}
