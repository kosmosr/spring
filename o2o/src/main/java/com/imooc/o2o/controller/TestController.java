package com.imooc.o2o.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class TestController {

    @RequestMapping("/test")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("test");

        return modelAndView;
    }
}
