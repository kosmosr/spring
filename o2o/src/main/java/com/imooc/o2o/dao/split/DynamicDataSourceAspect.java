package com.imooc.o2o.dao.split;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class DynamicDataSourceAspect {

    public DynamicDataSourceAspect() {
        System.out.println("Aspect !!");
    }

    @Pointcut("execution(* com.imooc.o2o.dao.*.select*(..))")
    public void readMethodPointcut() {
    }

    @Pointcut("execution(* com.imooc.o2o.dao.*.insert*(..)) || execution(* com.imooc.o2o.dao.*.update*(..)) || execution(* com.imooc.o2o.dao.*.delete*(..))")
    public void writeMethodPointcut() {
    }

    @Before("readMethodPointcut()")
    public void switchReadDataSource() {
        //System.out.println("============切换到读数据源===========");
        log.debug("=============切换到读数据源==========");
        DynamicDataSourceHolder.setDbType(DynamicDataSourceHolder.DB_SLAVE);
    }

    @Before("writeMethodPointcut()")
    public void switchWriteDataSource() {
        //System.out.println("=============切换到写数据源==========");
        log.debug("=============切换到写数据源==========");
        DynamicDataSourceHolder.setDbType(DynamicDataSourceHolder.DB_MASTER);
    }
}
