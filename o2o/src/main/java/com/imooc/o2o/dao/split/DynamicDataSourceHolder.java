package com.imooc.o2o.dao.split;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class DynamicDataSourceHolder {
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();
    public static final String DB_MASTER = "master";
    public static final String DB_SLAVE = "slave";

    public static String getDbType() {
        String db = threadLocal.get();
        if (db == null) {
            db = DB_MASTER;
        }

        return db;
    }

    public static void setDbType(String dbType) {
        log.debug("所使用的数据源为:" + dbType);
        threadLocal.set(dbType);
    }

    public static void clearDbtype() {
        threadLocal.remove();
    }
}
