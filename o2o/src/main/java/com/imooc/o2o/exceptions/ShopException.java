package com.imooc.o2o.exceptions;

public class ShopException extends RuntimeException {
    public ShopException(String message) {
        super(message);
    }
}
