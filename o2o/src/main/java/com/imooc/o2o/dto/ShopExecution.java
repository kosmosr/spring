package com.imooc.o2o.dto;

import com.imooc.o2o.enums.ShopStateEnum;
import com.imooc.o2o.pojo.Shop;
import lombok.Data;

import java.util.List;

@Data
public class ShopExecution {
    private int state;

    private String stateInfo;

    private int count;

    private Shop shop;

    private List<Shop> shopList;


    public ShopExecution(ShopStateEnum shopStateEnum) {
        this.state = shopStateEnum.getState();
        this.stateInfo = shopStateEnum.getStateInfo();
    }

    public ShopExecution(ShopStateEnum shopStateEnum, Shop shop) {
        this.state = shopStateEnum.getState();
        this.stateInfo = shopStateEnum.getStateInfo();
        this.shop = shop;
    }

    public ShopExecution(ShopStateEnum shopStateEnum, List<Shop> shopList) {
        this.state = shopStateEnum.getState();
        this.stateInfo = shopStateEnum.getStateInfo();
        this.shopList = shopList;
    }
}
