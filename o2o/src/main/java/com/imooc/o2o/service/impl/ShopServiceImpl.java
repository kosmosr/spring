package com.imooc.o2o.service.impl;

import com.imooc.o2o.dao.ShopMapper;
import com.imooc.o2o.dto.ShopExecution;
import com.imooc.o2o.enums.ShopStateEnum;
import com.imooc.o2o.exceptions.ShopException;
import com.imooc.o2o.pojo.Shop;
import com.imooc.o2o.service.ShopService;
import com.imooc.o2o.util.ImageUtil;
import com.imooc.o2o.util.PathUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.InputStream;
import java.util.Date;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private ShopMapper shopMapper;

    @Override
    public Shop getByShopId(Integer shopId) {
        return shopMapper.selectByPrimaryKey(shopId);
    }

    @Override
    public ShopExecution modifyShop(Shop shop, InputStream shopImg, String fileName) {
        if (shop == null && shop.getShopId() == null) {
            return new ShopExecution(ShopStateEnum.NULL_SHOP);
        } else {
            if (shopImg != null && StringUtils.hasText(fileName)) {
                Shop tempShop = shopMapper.selectByPrimaryKey(shop.getShopId());
                if (tempShop.getShopImg() != null) {
                    ImageUtil.deleteFileOrPath(tempShop.getShopImg());
                }
            }
            addShopImg(shop, shopImg, fileName);
        }
        int result = shopMapper.updateByPrimaryKeySelective(shop);
        if (result <= 0) {
            return new ShopExecution(ShopStateEnum.INNER_ERROR);
        } else {
            shop = shopMapper.selectByPrimaryKey(shop.getShopId());
            return new ShopExecution(ShopStateEnum.SUCCESS, shop);
        }
    }

    @Override
    @Transactional
    public ShopExecution addShop(Shop shop, InputStream shopImg, String fileName) {
        if (shop == null) {
            return new ShopExecution(ShopStateEnum.NULL_SHOP);
        }

        shop.setEnableStatus(ShopStateEnum.CHECK.getState());
        shop.setCreateTime(new Date());
        shop.setLastEditTime(new Date());
        int result = shopMapper.insert(shop);
        if (result <= 0) {
            throw new ShopException("店铺创建失败");
        } else {
            if (shopImg != null) {
                addShopImg(shop, shopImg, fileName);

                result = shopMapper.updateByPrimaryKey(shop);
                if (result <= 0) {
                    throw new ShopException("更新失败");
                }
            }
        }

        return new ShopExecution(ShopStateEnum.CHECK, shop);
    }

    private void addShopImg(Shop shop, InputStream shopImg, String fileName) {
        String dest = PathUtil.getShopImagePath(shop.getShopId());
        String shopImgAddr = ImageUtil.generateThumbnail(shopImg, dest, fileName);
        shop.setShopImg(shopImgAddr);
    }
}
