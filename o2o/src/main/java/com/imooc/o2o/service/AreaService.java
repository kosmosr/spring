package com.imooc.o2o.service;

import com.imooc.o2o.pojo.Area;

import java.util.List;

public interface AreaService {
    List<Area> getAreaList();
}
