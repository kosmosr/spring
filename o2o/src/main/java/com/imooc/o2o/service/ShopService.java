package com.imooc.o2o.service;

import com.imooc.o2o.dto.ShopExecution;
import com.imooc.o2o.pojo.Shop;

import java.io.InputStream;

public interface ShopService {
    Shop getByShopId(Integer shopId);

    ShopExecution modifyShop(Shop shop, InputStream shopImg, String fileName);

    ShopExecution addShop(Shop shop, InputStream shopImg, String fileName);
}
